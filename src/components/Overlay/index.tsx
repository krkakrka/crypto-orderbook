import styled from 'styled-components';

const Overlay = styled.div`
  z-index: 10;
  position: fixed;
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: rgba(1,1,1, 0.6);
`;

export default Overlay;