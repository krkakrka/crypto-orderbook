import _ from "lodash";
import { useCallback, useEffect, useState, useRef } from 'react';
import { ProductId } from '../../types';
import type {
  FeedMessage,
  PriceLevelSocket,
  PriceLevelSnapshotSocket,
  PriceLevelDeltaSocket,
  IUseCryptoOrderBookSocket,
  Price,
  Size,
  SocketRef
} from './types';
import { FeedEventType, isSnapshot, isDelta } from './types';
import { usePageVisibility } from "react-page-visibility";

const subscribeMessage = (productId: ProductId): FeedMessage => ({
  event: FeedEventType.SUBSCRIBE,
  feed: 'book_ui_1',
  product_ids: [productId]
});

const unsubscribeMessage = (productId: ProductId): FeedMessage => ({
  event: FeedEventType.UNSUBSCRIBE,
  feed: 'book_ui_1',
  product_ids: [productId]
});

/**Applies a delta message from the websocket to pricing levels snapshot.
 * Price sorting, maxTotal, spread and other calculations are done elsewhere later.
 */
const applyDeltaPriceLevels = (snapshotLevels: PriceLevelSocket[], deltaLevels: PriceLevelSocket[]): PriceLevelSocket[] => {
  const priceToSize: {[key: Price]: Size} = _.fromPairs(snapshotLevels);
  deltaLevels.forEach(([price, size]) => {
    priceToSize[price] = size;
  });
  // @ts-ignore
  const updatedLevels: PriceLevelSocket[] = (
    _.toPairs(priceToSize)
    .map(([priceStr, size]) => [parseFloat(priceStr), size])
    .filter(([price, size]) => size !== 0)
  );
  return updatedLevels;
}

const addDelta = (snapshot: PriceLevelSnapshotSocket, delta: PriceLevelDeltaSocket): PriceLevelSnapshotSocket => {
  return {
    ...snapshot,
    bids: applyDeltaPriceLevels(snapshot.bids, delta.bids),
    asks: applyDeltaPriceLevels(snapshot.asks, delta.asks)
  }
};

const FEED_URL = 'wss://www.cryptofacilities.com/ws/v1';

/**Hook that returns throttled data related to orderbook pricing levels.
 * The returned data in UNSORTED.
 * Throttling works by keeping a buffered snapshot of the pricing levels,
 * and updating it on every websocket delta update.
 * Then after refreshMs passes, the buffer is flushed to onMessage callback.
*/
const useCryptoOrderBookSocket = ({
    productId,
    onMessage,
    refreshMs = 1000
  }: IUseCryptoOrderBookSocket) => {
  const bufferedSnapshot = useRef<undefined|PriceLevelSnapshotSocket>();
  const [isLive, setIsLive] = useState(true);
  const isVisible = usePageVisibility()

  const updateBufferedSnapshot = (ev: MessageEvent) => {
    const messageData = JSON.parse(ev.data);
    if (isSnapshot(messageData)) {
      bufferedSnapshot.current = messageData;
    }
    if (isDelta(messageData) && bufferedSnapshot.current) {
      bufferedSnapshot.current = addDelta(bufferedSnapshot.current, messageData);
    }
  };

  useEffect(() => {
    const timerId = window.setInterval(() => {
      if (bufferedSnapshot.current) {
        onMessage(bufferedSnapshot.current);
      }
    }, refreshMs);
    return () => clearInterval(timerId);
  }, [onMessage, refreshMs]);

  const socketRef = useRef<SocketRef>();
  const messageQueueRef = useRef<FeedMessage[]>([]);
  useEffect(() => {
    // create new/close old socket if isLive changes
    if (isLive) {
      const socket = new WebSocket(FEED_URL);
      socketRef.current = {
        socket,
        // bufferes messages until socket is live and converts to str
        sendJsonMessage: (msg: FeedMessage) => {
          const message: string = JSON.stringify(msg)
          if (socket.readyState === 1) {
            socket.send(message);
          } else {
            messageQueueRef.current.push(msg);
          }
        }
      };
      socket.addEventListener('open', () => {
        // send queued messages
        while (messageQueueRef.current.length > 0) {
          const message = messageQueueRef.current.shift()
          if (message) {
            socketRef.current?.sendJsonMessage(message);
          }
        }
      })
      socket.addEventListener('message', updateBufferedSnapshot);
      // clear messages that havent been sent
      socket.addEventListener('close', () => {
        messageQueueRef.current = [];
      })
      return () => {
        socketRef.current?.socket.close();
      };
    } else {
      socketRef.current?.socket.close();
    }
  }, [isLive]);
  
  const subscribeTo = useCallback((productId: ProductId) => socketRef.current?.sendJsonMessage(subscribeMessage(productId)), []);
  const unsubscribeFrom = useCallback((productId: ProductId) => socketRef.current?.sendJsonMessage(unsubscribeMessage(productId)), []);

  useEffect(() => {
    if (isLive) {
      const otherProduct = productId === ProductId.PI_XBTUSD ? ProductId.PI_ETHUSD : ProductId.PI_XBTUSD;
      subscribeTo(productId);
      unsubscribeFrom(otherProduct);
    }
  }, [productId, isLive, subscribeTo, unsubscribeFrom])
  
  useEffect(() => {
    if (!isVisible) {
      socketRef.current?.socket.close();
      setIsLive(false);
    }
  }, [isVisible]);

  return {
    subscribeTo,
    unsubscribeFrom,
    isLive,
    reconnect: () => setIsLive(true)
  }
};

export default useCryptoOrderBookSocket;
export { applyDeltaPriceLevels };
export type {
  PriceLevelDeltaSocket,
  PriceLevelSocket,
  PriceLevelSnapshotSocket
}
