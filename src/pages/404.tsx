
import Link from 'next/link'
import styled from 'styled-components';
import Overlay from '../components/Overlay';

const Message = styled.p`
  color: white;
`;

const Custom404 = () => {
  return (
    <Overlay>
      <Message>{'The page you\'re looking for is not here...'}</Message>
      <Link href="/">
        <a>Home</a>
      </Link>
    </Overlay>
  );
}

export default Custom404;