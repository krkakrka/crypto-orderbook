import type { NextPage } from 'next'
import Orderbook from '../components/OrderBook';

const Home: NextPage = () => {
  return (
    <Orderbook />
  )
}

export default Home;
