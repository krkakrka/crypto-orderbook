import styled from 'styled-components';
import Head from 'next/head';
import ClipLoader from "react-spinners/ClipLoader";
import { ErrorBoundary } from 'react-error-boundary';
import { ProductId, OrderType, HasDataTestId } from '../../types';
import PriceLevelTable from '../PriceLevelTable';
import { MOBILE_BREAKPOINT_PX, BLUE } from '../../constants';
import { useTradingContext } from '../../contexts/TradingContext';
import usePricingLevels from '../../hooks/usePricingLevels';
import { calcMaxTotal, calcSpread } from '../../hooks/usePricingLevels';
import Overlay from '../Overlay';

const Layout = styled.div`
  display: grid;
  grid-template-rows: 2em calc(100vh - 6em) 4em;
  grid-template-columns: repeat(4, 1fr);
  grid-template-areas:
    "header spread spread productIdArea"
    "buys buys sells sells"
    "footer footer footer footer";
  overflow: hidden;

  @media (max-width: ${MOBILE_BREAKPOINT_PX}px) {
    grid-template-rows: 2em 2em auto 2em auto 4em;
    grid-template-columns: auto;
    grid-template-areas:
      "header"
      "productIdArea"
      "sells"
      "spread"
      "buys"
      "footer";
  }
`;

const Header = styled.div`
  grid-area: header;
  place-self: center;
  color: white;
`;

const Spread = styled.div`
  grid-area: spread;
  place-self: center;
  color: white;
`;


const Product = styled.div.attrs((props: HasDataTestId) => ({
  ['data-testid']: props['data-testid'],
}))`
  grid-area: productIdArea;
  place-self: center;
  color: white;
`;

const Footer = styled.div`
  grid-area: footer;
  place-self: center;
  display: flex;
`;

const Button = styled.button.attrs((props: HasDataTestId) => ({
  ['data-testid']: props['data-testid']
}))`
  background-color: ${BLUE};
  color: white;
  border-radius: 5%;
  border-style: initial;
  padding: 10px;

  &:hover {
    background-color: #362697;
  }
`;

const BuySide = styled.div`
  grid-area: buys;
`;

const SellSide = styled.div`
  grid-area: sells;
`;

const ErrorFallback = () => {
  return (
    <Overlay>Something went wrong, please contact support: support@someEmail.com</Overlay>
  );
}

const logError = (error: Error) => {
  // todo log to sentry or something
  console.error(error);
};

/**
 * Container/layout component for the order book - displays buys/sells and various other related
 * info.
 */
const OrderBook: React.FC = () => {
  const { productId, setProductId } = useTradingContext();
  const { pricingLevels, isLive, reconnect, clear } = usePricingLevels({
    maxRowCount: 25
  });
  const maxTotal = calcMaxTotal(pricingLevels);
  const spread = calcSpread(pricingLevels);
  const isLoading = pricingLevels.buyPriceLevels.length === 0 || pricingLevels.buyPriceLevels.length == 0;
  
  const handleToggleFeed = () => {
    clear();
    const otherProduct = productId === ProductId.PI_XBTUSD ? ProductId.PI_ETHUSD : ProductId.PI_XBTUSD;
    setProductId(otherProduct);
  };

  return (
    <ErrorBoundary FallbackComponent={ErrorFallback} onError={logError}>
      <Layout>
        <Head>
          <title>{productId}{!isLive ? ' (disconnected)': ''}</title>
        </Head>

        {(!isLive || isLoading) && (
        <Overlay>
          {!isLive && <Button onClick={reconnect}>Reconnect</Button>}
          {isLoading && (
            <div data-testid="loader">
              <ClipLoader loading={true} color={BLUE} data-testid="loader" />
            </div>
          )}
        </Overlay>
        )}

        <Header>Order Book</Header>
        
        <Spread>
          Spread {spread.toLocaleString(undefined, {minimumFractionDigits: 1, maximumFractionDigits: 1})}
        </Spread>

        <Product data-testid="productId">{productId}</Product>

        <BuySide>
          <PriceLevelTable priceLevels={pricingLevels.buyPriceLevels} type={OrderType.BUY} maxTotal={maxTotal} />
        </BuySide>
        <SellSide>
          <PriceLevelTable priceLevels={pricingLevels.sellPriceLevels} type={OrderType.SELL} maxTotal={maxTotal} />
        </SellSide>

        <Footer>
          <Button onClick={handleToggleFeed} data-testid="toggle">Toggle product</Button>
        </Footer>
      </Layout>
    </ErrorBoundary>
  );
}

export default OrderBook;
