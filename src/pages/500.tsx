
import Link from 'next/link'
import styled from 'styled-components';
import Overlay from '../components/Overlay';

const Message = styled.p`
  color: white;
`;

const Custom500 = () => {
  return (
    <Overlay>
      <Message>{'Something bad happened, please try again later...'}</Message>
      <Link href="/">
        <a>Home</a>
      </Link>
    </Overlay>
  );
}

export default Custom500;
