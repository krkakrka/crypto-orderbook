import { calcMaxTotal, calcSpread } from './index';
import { pricingLevels } from './mockData';

test('calculate maxTotal correctly', () => {
  const maxTotal = calcMaxTotal(pricingLevels);
  expect(maxTotal).toEqual(546787);
});

test('calc spread correctly', () => {
  const spread = calcSpread(pricingLevels);
  expect(spread).toEqual(24.5);
})
