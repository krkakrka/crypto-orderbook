import type { AppProps } from 'next/app'
import 'normalize.css';
import GlobalStyle from '../GlobalStyle';
import TradingContextProvider from '../contexts/TradingContext';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <TradingContextProvider>
      <GlobalStyle />
      <Component {...pageProps} />
    </TradingContextProvider>
  );
}

export default MyApp
