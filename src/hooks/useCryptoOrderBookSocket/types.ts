import { ProductId } from '../../types';

type Price = number;
type Size = number;
type PriceLevelSocket = [Price, Size];
interface PriceLevelDeltaSocket {
  feed: string,
  bids: PriceLevelSocket[],
  asks: PriceLevelSocket[],
  product_id: string  
}
interface PriceLevelSnapshotSocket extends PriceLevelDeltaSocket {
  numLevels: number,
}
interface IUseCryptoOrderBookSocket {
  productId: ProductId,
  onMessage: (event: PriceLevelSnapshotSocket) => void,
  onError?: (event: WebSocketEventMap['error']) => void,
  refreshMs?: number
}

interface SocketRef {
  socket: WebSocket,
  sendJsonMessage(msg: FeedMessage): void
}
const isSnapshot = (message: PriceLevelSnapshotSocket|PriceLevelDeltaSocket): message is PriceLevelSnapshotSocket => {
  return (
    'numLevels' in (message as PriceLevelSnapshotSocket) &&
    'bids' in (message as PriceLevelSnapshotSocket)
  );
}
const isDelta = (message: PriceLevelSnapshotSocket|PriceLevelDeltaSocket): message is PriceLevelDeltaSocket => {
  return (
    !('numLevels' in (message as PriceLevelDeltaSocket)) &&
    'bids' in (message as PriceLevelDeltaSocket)
  );
}

enum FeedEventType {
  SUBSCRIBE = 'subscribe',
  UNSUBSCRIBE = 'unsubscribe',
}

interface FeedMessage {
  event: FeedEventType,
  feed: 'book_ui_1',
  product_ids: [ProductId]
}

export { FeedEventType, isSnapshot, isDelta }
export type {
  FeedMessage,
  PriceLevelSocket,
  PriceLevelSnapshotSocket,
  PriceLevelDeltaSocket,
  Price,
  Size,
  IUseCryptoOrderBookSocket,
  SocketRef
}