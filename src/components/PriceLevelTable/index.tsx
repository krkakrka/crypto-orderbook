
import styled from 'styled-components';
import { OrderType, PriceLevel, HasDataTestId } from '../../types';
import PriceLevelRow from './PriceLevelRow';
import { MOBILE_BREAKPOINT_PX } from '../../constants';

interface PriceLevelTableProps {
  priceLevels: PriceLevel[],
  type: OrderType,
  maxTotal: number
}
interface PriceProps {
  type: OrderType
}
interface HeadRowProps {
  type: OrderType
}
interface DataRowsContainerProps extends HasDataTestId {
  type: OrderType
}
enum Color {
  RED = 'rgb(16, 125, 91)',
  GREEN = 'rgb(170, 46, 50)'
}

const Layout = styled.div`
  text-align: right;
  display: flex;
  flex-direction: column;
`;
const Total = styled.div`
  color: white;
  flex: 1;
`;
const Size = styled.div`
  color: white;
  flex: 1;
`;
const Price = styled.div<PriceProps>`
  color: ${props => (props.type === OrderType.BUY ? Color.RED : Color.GREEN)};
  flex: 1;
`;
const ColumnLabel = styled.div`
  color: #6894aa;
  text-transform: uppercase;
  flex: 1;
`;
const HeadRow = styled.div<HeadRowProps>`
  display: flex;
  flex-direction: ${props => props.type === OrderType.SELL ? 'row-reverse' : 'initial'};
  padding-right: 4em;
  border-top: 1px solid #6894aa;

  @media (max-width: ${MOBILE_BREAKPOINT_PX}px) {
    display: ${props => props.type === OrderType.BUY ? 'none' : 'flex'};
    
  }
`;
const DataRowsContainer = styled.div.attrs((props: DataRowsContainerProps) => ({
  ['data-testid']: props['data-testid']
}))`
  text-align: right;
  display: flex;
  flex-direction: column;

  @media (max-width: ${MOBILE_BREAKPOINT_PX}px) {
    flex-direction: ${(props: DataRowsContainerProps) => props.type === OrderType.SELL ? 'column-reverse' : 'column'}
  }
`;

const priceLevelToKey = (level: PriceLevel) => `${level.price}-${level.size}-${level.total}`;

/**
 * Displays the prices levels for buy/sell side.
 * Price levels MUST be sorted appropriately based on type.
 * Sorting isn't included because maxTotal calculation requires both buy and sell
 * sides to be sorted and their totals aggregated.
 * In other words, that calculation should be don in advanced.
 */
const PriceLevelTable: React.FC<PriceLevelTableProps> = ({ priceLevels, maxTotal, type }) => {
  return (
    <Layout>
      <HeadRow type={type}>
        <ColumnLabel>Total</ColumnLabel>
        <ColumnLabel>Size</ColumnLabel>
        <ColumnLabel>Price</ColumnLabel>
      </HeadRow>
      <DataRowsContainer type={type} data-testid="priceRowsContainer">
        {priceLevels.map(priceLevel => (
        <PriceLevelRow
          priceLevel={priceLevel}
          type={type}
          maxTotal={maxTotal}
          key={priceLevelToKey(priceLevel)}
        >
          <Total>{priceLevel.total.toLocaleString()}</Total>
          <Size>{priceLevel.size.toLocaleString()}</Size>
          <Price type={type}>
            {priceLevel.price.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}
          </Price>
        </PriceLevelRow>
        ))}
      </DataRowsContainer>
    </Layout>
  )
};

export default PriceLevelTable;
