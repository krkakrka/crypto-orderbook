describe('My orderbook', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  })
  it('shows a loader when loading and hides it later', () => {
    cy.get('[data-testid=loader]').should('exist');
    cy.get('[data-testid=loader]').should('not.exist', {timeout: 5000});
  })
  it('displayes buys and sells after loading', () => {
    cy.get('[data-testid=loader]').should('not.exist', {timeout: 5000});
    cy.get('[data-testid=priceRowsContainer]').children().its('length').should('be.gte', 10);
  })
  it('toggled the feed when toggle button is pressed', () => {
    // wait till loaded
    cy.get('[data-testid=loader]').should('not.exist', {timeout: 5000});
    cy.get('[data-testid=productId]').should('have.text', 'PI_XBTUSD');

    cy.get('button[data-testid=toggle]').click()
    
    // wait till loaded
    cy.get('[data-testid=loader]').should('exist');
    cy.get('[data-testid=loader]').should('not.exist', {timeout: 5000});
    cy.get('[data-testid=productId]').should('have.text', 'PI_ETHUSD');
  })
  it('changes data in real time', () => {
    // wait till loaded
    cy.get('[data-testid=loader]').should('not.exist', {timeout: 5000});
    cy.get('[data-testid=priceRowsContainer]').children().then(el => {
      cy.wrap(el.text()).as('priceLevelsSnapshot');
    });
    cy.wait(1000);
    cy.get('[data-testid=priceRowsContainer]').children().then(el => {
      expect(el.text()).not.to.equal(cy.get('@priceLevelsSnapshot'));
    });
  });
});
