export enum ProductId {
  PI_XBTUSD = 'PI_XBTUSD',
  PI_ETHUSD = 'PI_ETHUSD'
}
export enum OrderType {
  BUY, SELL
}
export interface PriceLevel {
  price: number,
  size: number,
  total: number
}
export interface HasDataTestId {
  ['data-testid']: string
}