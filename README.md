# Orderbook

## [>>Demo<<](https://crypto-orderbook-g28qeqkbs-krkakrka.vercel.app/)

![Alt text](/screenshots/desktop.png "Desktop orderbook")
![Alt text](/screenshots/reconnect.png "Reconnection")
![Alt text](/screenshots/mobile.png "Mobile orderbook")

## Features
- Shows a live orderbook for XBTUSD or ETHUSD.
- Disconnects when a tab is hidden to reduce data usage.

## Tech
- Typescript.
- React, hooks, styled components.
- Nextjs (minimal).
- Jest.
- Cypress.

## Code organisation
Application src organised by module types:
- `components/` - React components.
- `contexts/` - React contexts.
- `hooks/` - React hooks.
- `pages/` - NextJS pages - concerned with static/dynamic rendering, routing..

In the future, if necessary, further separation could be done by domains/features.
Also, something like `utilities/` or `services/` might appear as well, would contain React agnostic typescript logic.

## Running locally
```
npm install && npm run dev
```

## Testing
#### Jest
```
npm test
```
#### Cypress
```
npm run cypress
```


## Dev notes
- Little concern given to various NextJS optimisations, the focus is on the live stream.
- TradingContext - seems small, but will grow once other features are added.
- PriceLevelRow styling is handled without styled components for better performance.
- Dynamic row count is currently on another branch, work in progress.

## Ideas/todos
- add visual testing, especially responsive layout changes.
- nextjs prod config (headers, etc).
- adaptive socket throttle based on browser performance.
- move socket to webworker?
- adaptive price level row count based on screen height;
- Add tests for tab focus that disabled the socket when not visible.
- MAYBE refactor back usePricingLevels to a reducer?
- Move some variables to .env files (like the websocket url).
- fir minor type hacks.
- toggle footer should be fixed on mobile, pricing levels scrollable if they don't fit.
- reconnect overlay doesn't fully overlay on mobile chrome.
