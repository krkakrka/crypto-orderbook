import _ from 'lodash';
import { useState, useCallback } from 'react';
import { useTradingContext } from '../../contexts/TradingContext';
import { OrderType, PriceLevel } from '../../types';
import useCryptoOrderBookSocket, { PriceLevelSocket, PriceLevelSnapshotSocket } from '../useCryptoOrderBookSocket';

interface PricingLevelsState {
  buyPriceLevels: PriceLevel[],
  sellPriceLevels: PriceLevel[]
}

interface UsePricingLevels {
  pricingLevels: PricingLevelsState,
  isLive: boolean,
  reconnect: () => void,
  clear: () => void
}

const INITIAL_STATE = {
  buyPriceLevels: [],
  sellPriceLevels: []
}

const reduceSocketPriceLevelsToPriceLevels = (acc: PriceLevel[], [price, size]: PriceLevelSocket, index: number) => {
  const total = index === 0 ? size : acc[index-1].total + size
  return [...acc, {price, size, total}];
}

const sortPriceDesc = (a: PriceLevelSocket, b: PriceLevelSocket): number => b[0]-a[0];
const sortPriceAsc = (a: PriceLevelSocket, b: PriceLevelSocket): number => a[0]-b[0];

const calcPriceLevelsFromSnapshot = (priceLevels: PriceLevelSocket[], type: OrderType): PriceLevel[] => {
  return (
    priceLevels
      .sort(type === OrderType.BUY ?  sortPriceDesc : sortPriceAsc)
      .reduce(reduceSocketPriceLevelsToPriceLevels, [])
  )
}

/**
 * Calculated the maximum total size from all price levels.
 * Assumes pricing levels are sorted appropriately, decreasing for buys, increasing for sells.
 */
const calcMaxTotal = (state: PricingLevelsState): number => (
  Math.max(
    _.last(state.buyPriceLevels)?.total || 0,
    _.last(state.sellPriceLevels)?.total || 0
  )
)

/**
 * Calculated the difference between the highest buy and lowest sell.
 * Assumes pricing levels are sorted appropriately, decreasing for buys, increasing for sells.
 */
const calcSpread = (state: PricingLevelsState): number => (
  (state.sellPriceLevels[0]?.price || 0) - (state.buyPriceLevels[0]?.price || 0)
);

interface IUsePricingLevels {
  maxRowCount: number,
}

/**Hook that returns a feed of order book pricing levels.
 * Use with calcMaxTotal and calcSpread.
 */
const usePricingLevels = ({
  maxRowCount = 25,
}: IUsePricingLevels): UsePricingLevels => {
  const [pricingLevels, setPriceLevels] = useState<PricingLevelsState>(INITIAL_STATE);
  const { productId } = useTradingContext();
  const onMessage = useCallback((message: PriceLevelSnapshotSocket) => {
    setPriceLevels({
      buyPriceLevels: calcPriceLevelsFromSnapshot(message.bids, OrderType.BUY),
      sellPriceLevels: calcPriceLevelsFromSnapshot(message.asks, OrderType.SELL)
    })
  }, []);
  const { isLive, reconnect } = useCryptoOrderBookSocket({
    productId,
    onMessage,
    refreshMs: 500
  });
  const clear = () => setPriceLevels({
    buyPriceLevels: [],
    sellPriceLevels: []
  });
  
  return {
    pricingLevels: {
      buyPriceLevels: pricingLevels.buyPriceLevels.slice(0, maxRowCount),
      sellPriceLevels: pricingLevels.sellPriceLevels.slice(0, maxRowCount)
    },
    clear,
    isLive,
    reconnect
  }
};

export default usePricingLevels;
export {
  calcMaxTotal,
  calcSpread
}