import { useMediaQuery } from 'react-responsive';
import styled from 'styled-components';
import { OrderType, PriceLevel } from '../../types';
import { MOBILE_BREAKPOINT_PX } from '../../constants';

const GREEN = 'rgba(16, 125, 91, 30%)';
const RED = 'rgba(170, 46, 50, 30%)';

const calcFillPercentage = (priceLevel: PriceLevel, maxTotal: number) => (maxTotal - priceLevel.total) / maxTotal;

const calcGradientDirection = (type: OrderType, isMobile: boolean) => {
  if (type == OrderType.SELL) return 'to left';
  if (type == OrderType.BUY && !isMobile) return 'to right';
  if (type == OrderType.BUY && isMobile) return 'to left';
};

const calcRowDirection = (type: OrderType, isMobile: boolean) => {
  if (type === OrderType.SELL && !isMobile) return 'row-reverse';
  if (type === OrderType.BUY && isMobile) return 'row-reverse';
  return 'initial';
}

interface PriceLevelRowProps {
  priceLevel: PriceLevel,
  type: OrderType,
  maxTotal: number
}

// too slow, too many changes
// const PriceLevelRow = styled.div<PriceLevelRowProps>`
//   display: flex;
//   padding-right: 4em;
//   background:
//     rgba(0,0,0,0)
//     linear-gradient(
//       to ${props => props.type === OrderType.BUY ? 'right' : 'left'}
//       transparent
//         ${props => 100 * calcFillPercentage(props.priceLevel, props.maxTotal)}
//       ${props => props.type === OrderType.BUY ? GREEN: RED}
//         ${props => 100 * calcFillPercentage(props.priceLevel, props.maxTotal)}
//     )
  
//   @media (max-width: ${MOBILE_BREAKPOINT_PX}px) {
//     background:
//       rgba(0,0,0,0)
//       linear-gradient(
//         to left
//         transparent
//           ${props => 100 * calcFillPercentage(props.priceLevel, props.maxTotal)}
//         ${props => props.type === OrderType.BUY ? GREEN: RED}
//           ${props => 100 * calcFillPercentage(props.priceLevel, props.maxTotal)}
//       )
//   }
// `;

// todo if possible remove useMediaQuery
const PriceLevelRow: React.FC<PriceLevelRowProps> = ({ priceLevel, type, maxTotal, children }) => {
  const isMobile = useMediaQuery({ query: `(max-width: ${MOBILE_BREAKPOINT_PX}px)` })
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: calcRowDirection(type, isMobile),
        paddingRight: '4em',
        background: `
          rgba(0, 0, 0, 0)
          linear-gradient(
            ${calcGradientDirection(type, isMobile)},
            transparent
              ${100 * calcFillPercentage(priceLevel, maxTotal)}%,
            ${type === OrderType.BUY ? GREEN: RED}
              ${100 * calcFillPercentage(priceLevel, maxTotal)}%
          )`
      }}
    >
      {children}
    </div>
  );
};

export default PriceLevelRow;
