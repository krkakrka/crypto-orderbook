import { applyDeltaPriceLevels } from './index';
import type { PriceLevelSocket } from './index';

describe('applyDeltaPriceLevels', () => {
  const pricingLevels: PriceLevelSocket[] = [
    [65591,3600],
    [65591.5,9807],
    [65593,290],
    [65594.5, 3034],
    [65599, 2500]
  ]
  test('should remove all levels when delta has all zeros', () => {
    const allZerosDelta: PriceLevelSocket[] = pricingLevels.map(([price, size]) => [price, 0]);
    const snapshotAfterDelta = applyDeltaPriceLevels(pricingLevels, allZerosDelta);
    expect(snapshotAfterDelta).toEqual([])
  });
  test('should double all sizes', () => {
    const doubleSizesDelta: PriceLevelSocket[] = pricingLevels.map(([price, size]) => [price, size * 2]);
    const snapshotAfterDelta = applyDeltaPriceLevels(pricingLevels, doubleSizesDelta).sort();
    // happens to be identical to the delta.
    const doubleSizesPricingLevels = doubleSizesDelta.sort()
    expect(snapshotAfterDelta).toEqual(doubleSizesPricingLevels)
  });
})
