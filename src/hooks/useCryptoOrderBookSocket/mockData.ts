import type { PriceLevelSocket } from './index';

export const pricingLevels: PriceLevelSocket[] = [
  [65591,3600],
  [65591.5,9807],
  [65593,290],
  [65594.5, 3034],
  [65599, 2500]
]

export const allZerosDelta: PriceLevelSocket[] = pricingLevels.map(([price, size]) => [price, 0]);
export const doubleSizesDelta: PriceLevelSocket[] = pricingLevels.map(([price, size]) => [price, size * 2]);
// happens to be identical to the delta.
export const doubleSizesPricingLevels = doubleSizesDelta;