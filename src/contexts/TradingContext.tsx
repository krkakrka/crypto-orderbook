import { createContext, useState, useContext } from 'react';
import _ from 'lodash';
import { ProductId } from '../types';

interface ITradingContext {
  productId: ProductId,
  setProductId: (productId: ProductId) => void
}
const INITIAL_PRODUCT = ProductId.PI_XBTUSD;
const INITIAL_TRADING_CONTEXT = {
  productId: INITIAL_PRODUCT,
  setProductId: _.noop
}
const TradingContext = createContext<ITradingContext>(INITIAL_TRADING_CONTEXT);

const TradingContextProvider: React.FC = ({ children }) => {
  const [productId, setProductId] = useState(INITIAL_PRODUCT);
  return (
    <TradingContext.Provider value={{
        productId,
        setProductId
      }}>
      {children}
    </TradingContext.Provider>
  )
};

export default TradingContextProvider;
export const useTradingContext = () => useContext(TradingContext);
